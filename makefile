.PHONY: clean test

bin = bin
src = src
test = test

$(bin)/%.o: $(src)/%.asm
	nasm -f elf64 -o $@ $< 

program: $(bin)/main.o $(bin)/dict.o $(bin)/lib.o
	ld -o $@ $^

bin/main.o: $(src)/colon.inc $(src)/words.inc $(src)/dict.inc $(src)/lib.inc

clean:
	cd $(bin)
	rm -f bin/*
	rm -f program

test: program
	./test/test.sh
	
