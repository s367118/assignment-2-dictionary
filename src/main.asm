%define SYS_OUT 1
%define SYS_ERR 2
%define KEY_OFFSET 8
%define INPUT_MAX_LEN 256

global _start

%include "src/colon.inc"
%include "src/words.inc"

%include "src/lib.inc"
%include "src/dict.inc"

section .rodata
not_found_msg: db "There is no data associated with such key.", `\n`, 0

section .bss
input: resb INPUT_MAX_LEN

section .text
_start:
	mov rdi, input
	mov rsi, INPUT_MAX_LEN
	call read_word
	test rdx, rdx
	je .exit

	mov rdi, rax
	mov rsi, next_el	
	call find_word
	test rax, rax
	je .not_found
	
	add rax, KEY_OFFSET
	push rax
	mov rdi, rax
	call string_length
	pop rdi
	add rdi, rax		;value		
	inc rdi				;string_length does not count null term
	mov rsi, SYS_OUT		
	call print_string
	call print_newline

.exit:
	xor rdi, rdi
	call exit
	
.not_found:
	mov rdi, not_found_msg
	mov rsi, SYS_ERR
	call print_string
	jmp .exit
