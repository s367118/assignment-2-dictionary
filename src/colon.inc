%define next_el 0

%macro colon 2
%%label: dq next_el
%define next_el %%label
db %1, 0
%2:
%endmacro
