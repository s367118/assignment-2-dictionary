%define KEY_OFFSET 8

global find_word

extern string_equals

section .text

;rdi - null term strnig pointer -> r13
;rsi - element pointer -> r12
find_word:
	push r12
	push r13
	
	mov r12, rsi
	mov r13, rdi

.next_element:
	test r12, r12				;end of list				
	je .not_found				;or empty list
	
	mov rsi, r12				;current_el
	add rsi, KEY_OFFSET
	mov rdi, r13	
	call string_equals
	test rax, rax
	jne .found
	
	mov r12, [r12]	
	jmp .next_element

.found:
	mov rax, r12

.end:
	pop r13
	pop r12
	ret

.not_found:
	xor rax, rax
	jmp .end
