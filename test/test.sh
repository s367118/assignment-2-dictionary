#!/bin/bash
mv src/words.inc test/words.inc
cp test/test_words.inc src/words.inc
make program

k1="test_1"
k2="wrong_key"

cd test
echo ""
echo $k1 | ./../program 1>test_res 2>test_res
echo $k2 | ./../program 1>>test_res 2>>test_res
if [ $(diff -Bw test_res test_expect | wc -c) -eq 0 ];
then
	echo "$0 OK"
else
	echo "Give:"
	echo $(k1)
	echo $(k2)
	echo "-----------"
	echo "Expect:"
	cat test_expect
	echo "-----------"
	echo "Get:"
	cat test_res
fi
echo ""
cd ..

rm src/words.inc
mv test/words.inc src/words.inc
touch src/words.inc
make program
